#include "c2.hlsl"

cbuffer InputConstants : register(b0) {
    uint2 startingKey;
	uint2 inBlock1;
	uint2 inBlock2;
}

struct OutputData {
    uint2 foundKey;
    bool findStatus;
};

RWStructuredBuffer<OutputData> outputData : register(u0);

[numthreads(1024, 1, 1)]
void main(uint3 threadID : SV_DispatchThreadID) {
    uint globalThreadID = threadID.x;
    uint2 key = uint2(startingKey.x, startingKey.y + globalThreadID);

    c2_dcbc_state state;

    c2_dcbc_init(key, state);

    uint2 block1 = c2_dcbc_block(inBlock1, key, state);
    if ((block1.x & 0xFF000000) == 0) {
        uint2 block2 = c2_dcbc_block(inBlock2, key, state);
        if ((block2.x & 0xFFFF) == 0 && (block2.y) == 0) {
            outputData[0].foundKey = key;
            outputData[0].findStatus = true;
        }
    }
}
