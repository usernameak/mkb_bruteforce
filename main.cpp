#include "renderdoc_app.h"
#include <array>
#include <atomic>
#include <c2.h>
#include <cassert>
#include <cstdio>
#include <d3d11.h>
#include <dxgi.h>
#include <new>
#include <chrono>
#include <windows.h>
#include <wrl/client.h>

#define PRINT_ERROR(message, errorcode) \
    fprintf(stderr, message ": error %#08x\n", (uint32_t)(errorcode))

#define SHOW_ERROR(message, errorcode)   \
    do {                                 \
        PRINT_ERROR(message, errorcode); \
        return E_FAIL;                   \
    } while (false)

#define CHECK_HRESULT(message, hr)                 \
    do {                                           \
        if (HRESULT _hres = (hr); FAILED(_hres)) { \
            PRINT_ERROR(message, _hres);           \
            return _hres;                          \
        }                                          \
    } while (false)

struct InputShaderData {
    uint32_t startingKey_hi;
    uint32_t startingKey_lo;
    uint32_t block1_a;
    uint32_t block1_b;
    uint32_t block2_a;
    uint32_t block2_b;
};

struct OutputShaderData {
    uint32_t key_hi;
    uint32_t key_lo;
    uint32_t status;
};

RENDERDOC_API_1_6_0 *queryRenderDocAPI() {
    RENDERDOC_API_1_6_0 *renderDocAPI = nullptr;

    // At init, on windows
    if (HMODULE mod = GetModuleHandleA("renderdoc.dll")) {
        auto RENDERDOC_GetAPI =
            reinterpret_cast<pRENDERDOC_GetAPI>(GetProcAddress(mod, "RENDERDOC_GetAPI"));
        int ret = RENDERDOC_GetAPI(eRENDERDOC_API_Version_1_6_0, reinterpret_cast<void **>(&renderDocAPI));
        assert(ret == 1);
        return renderDocAPI;
    }

    return nullptr;
}

enum EnumAction {
    ACTION_LIST_ADAPTERS  = 0,
    ACTION_RUN_BRUTEFORCE = 1
};

HRESULT listAdapters(const Microsoft::WRL::ComPtr<IDXGIFactory1> &factory) {
    Microsoft::WRL::ComPtr<IDXGIAdapter1> adapter;
    for (UINT i = 0;; i++) {
        HRESULT hr = factory->EnumAdapters1(i, &adapter);
        if (hr == DXGI_ERROR_NOT_FOUND) break;
        CHECK_HRESULT("Could not enum DXGI adapters", hr);

        DXGI_ADAPTER_DESC1 desc{};
        adapter->GetDesc1(&desc);
        printf("Adapter #%d: %ls\n", i, desc.Description);
    }
    return S_OK;
}

static uint64_t calculateBatchesPerSecond(uint64_t input) {
    return input == 0 ? 0 : 0x100000000ull * 1000 / input;
}

class JobGroup {
    uint64_t m_blockStart;
    uint64_t m_curBlock;
    uint64_t m_blockEnd;

    uint64_t m_runningMilliseconds;

public:
    JobGroup(uint64_t blockStart, uint64_t blockEnd)
        : m_blockStart(blockStart),
          m_curBlock(blockStart),
          m_blockEnd(blockEnd),
          m_runningMilliseconds(0) {}

    bool findNextBlock(uint64_t &block) {
        if (m_curBlock >= m_blockEnd) return false;
        block = m_curBlock++;
        return true;
    }

    uint64_t additionalAverageByLastBatchTime(uint64_t lastBatchTime) {
        m_runningMilliseconds += lastBatchTime;

        return calculateCurrentAverage();
    }

    [[nodiscard]] uint64_t calculateCurrentAverage() const {
        if (m_curBlock == m_blockStart) return 0;

        uint64_t average = (m_runningMilliseconds / (m_curBlock - m_blockStart));
        return (average < UINT64_MAX ? average : UINT64_MAX);
    }

    [[nodiscard]] uint64_t getElapsedTime() const { return m_runningMilliseconds; }

    [[nodiscard]] uint64_t getBlocksRemaining() const { return m_blockEnd - m_curBlock; }

    [[nodiscard]] uint64_t estimateTimeRemaining() const { return getBlocksRemaining() * calculateCurrentAverage(); }
};

enum EnumTimeUnit {
    TIME_UNIT_MILLISECONDS,
    TIME_UNIT_SECONDS,
    TIME_UNIT_MINUTES,
    TIME_UNIT_HOURS,
    TIME_UNIT_DAYS,
    NUM_TIME_UNITS
};

static const char *timeUnitSymbol[NUM_TIME_UNITS]   = { "ms", "s", "m", "h", "d" };
static const uint64_t timeUnitLimit[NUM_TIME_UNITS] = { 1000, 60, 60, 24, 0 };

static const char *humanReadableTime(char *out, size_t bufSize, uint64_t value) {
#define PER_BUFSIZE 64
    char perBuf[NUM_TIME_UNITS][PER_BUFSIZE];
    uint64_t perUnit[NUM_TIME_UNITS];

    if (value < 0) {
        snprintf(out, bufSize, "?");
        return out;
    }

    memset(out, 0, sizeof(*out) * bufSize);
    memset(perBuf, 0, sizeof(perBuf));
    memset(perUnit, 0, sizeof(perUnit));

    for (int i = 0; i < NUM_TIME_UNITS; i++) {
        if (value == 0) {
            break;
        }

        uint64_t limit = timeUnitLimit[i];

        if (limit == 0) {
            perUnit[i] = value;
        } else {
            perUnit[i] = value % limit;
            value      = value / limit;
        }

        snprintf(perBuf[i], PER_BUFSIZE - 1, "%llu%s", perUnit[i], timeUnitSymbol[i]);

        if (limit == 0) {
            break;
        }
    }

    for (int i = NUM_TIME_UNITS - 1; i >= 0; i--) {
        if (perUnit[i] > 0) {
            if (strlen(out) > 0) {
                strncat(out, " ", bufSize);
            }

            strncat(out, perBuf[i], bufSize);
        }
    }

    return out;
#undef PER_BUFSIZE
}

static const char *humanReadableUnits(char *out, size_t bufSize, uint64_t value, const char *symbol) {
    double dval         = (double)value;
    int i               = 0;
    const char *units[] = { "", "k", "M", "G", "T", "P", "E", "Z", "Y" };
    while (dval > 1000) {
        dval /= 1000;
        i++;
    }

    snprintf(out, bufSize, "%.*lf %s%s", i, dval, units[i], symbol);
    return out;
}

constexpr static uint32_t packBytes(const uint8_t *data) {
    return (static_cast<uint32_t>(data[0]) << 24u) |
           (static_cast<uint32_t>(data[1]) << 16u) |
           (static_cast<uint32_t>(data[2]) << 8u) |
           (static_cast<uint32_t>(data[3]) << 0u);
}

bool validateKey(const uint8_t *tkure, uint64_t key) {
    uint8_t tkureTmp[64];
    memcpy(tkureTmp, tkure, sizeof(tkureTmp));
    c2_dcbc(tkureTmp, key, 64);

    if (tkureTmp[0] == 0 &&
        tkureTmp[0xA] == 0 &&
        tkureTmp[0xB] == 0 &&
        tkureTmp[0xC] == 0 &&
        tkureTmp[0xD] == 0 &&
        tkureTmp[0xE] == 0 &&
        tkureTmp[0xF] == 0 &&
        tkureTmp[0x38] == 0x01 &&
        tkureTmp[0x39] == 0x23 &&
        tkureTmp[0x3A] == 0x45 &&
        tkureTmp[0x3B] == 0x67 &&
        tkureTmp[0x3C] == 0x89 &&
        tkureTmp[0x3D] == 0xAB &&
        tkureTmp[0x3E] == 0xCD &&
        tkureTmp[0x3F] == 0xEF) {

        return true;
    }

    return false;
}

void findValidKeysInBlock(const uint8_t *tkure, uint32_t blockNumber) {
    uint64_t startingKey = static_cast<uint64_t>(blockNumber) << 32;
    uint64_t endingKey   = static_cast<uint64_t>(blockNumber + 1) << 32;

    for (uint64_t key = startingKey; key < endingKey; key++) {
        validateKey(tkure, key);
    }
}

HRESULT runBruteforce(
    const Microsoft::WRL::ComPtr<IDXGIFactory1> &factory,
    const Microsoft::WRL::ComPtr<IDXGIAdapter1> &adapter,
    JobGroup &jobGroup,
    int flushFrequency,
    const uint8_t *tkure
) {

    RENDERDOC_API_1_6_0 *rdocAPI = queryRenderDocAPI();

    D3D_FEATURE_LEVEL featureLevels[]{
        D3D_FEATURE_LEVEL_12_0,
        D3D_FEATURE_LEVEL_11_1,
        D3D_FEATURE_LEVEL_11_0
    };

    Microsoft::WRL::ComPtr<ID3D11Device> device;
    Microsoft::WRL::ComPtr<ID3D11DeviceContext> deviceContext;

    UINT deviceFlags = 0;
#if (defined(DEBUG) || defined(_DEBUG))
    deviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif /* (defined(DEBUG) || defined(_DEBUG)) */
    HRESULT hr = D3D11CreateDevice(
        adapter.Get(),
        D3D_DRIVER_TYPE_UNKNOWN, nullptr,
        deviceFlags,
        featureLevels, std::size(featureLevels),
        D3D11_SDK_VERSION,
        &device,
        nullptr,
        &deviceContext
    );
    CHECK_HRESULT("Cannot create device", hr);

    if (rdocAPI) rdocAPI->StartFrameCapture(device.Get(), nullptr);

    Microsoft::WRL::ComPtr<ID3D11Buffer> inputBuffer;
    {
        D3D11_BUFFER_DESC bufferDesc{};
        bufferDesc.ByteWidth           = 64;
        bufferDesc.Usage               = D3D11_USAGE_DYNAMIC;
        bufferDesc.BindFlags           = D3D11_BIND_CONSTANT_BUFFER;
        bufferDesc.CPUAccessFlags      = D3D11_CPU_ACCESS_WRITE;
        bufferDesc.MiscFlags           = 0;
        bufferDesc.StructureByteStride = 0;

        hr = device->CreateBuffer(&bufferDesc, nullptr, &inputBuffer);
        CHECK_HRESULT("Cannot create input buffer", hr);
    }

    Microsoft::WRL::ComPtr<ID3D11Buffer> outputBuffer;
    Microsoft::WRL::ComPtr<ID3D11Buffer> outputBufferCopy;
    Microsoft::WRL::ComPtr<ID3D11Buffer> outputBufferClear;
    Microsoft::WRL::ComPtr<ID3D11UnorderedAccessView> outputUAV;
    {
        D3D11_BUFFER_DESC bufferDesc{};
        bufferDesc.ByteWidth           = 12;
        bufferDesc.Usage               = D3D11_USAGE_DEFAULT;
        bufferDesc.BindFlags           = D3D11_BIND_UNORDERED_ACCESS;
        bufferDesc.CPUAccessFlags      = 0;
        bufferDesc.MiscFlags           = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;
        bufferDesc.StructureByteStride = 12;

        hr = device->CreateBuffer(&bufferDesc, nullptr, &outputBuffer);
        CHECK_HRESULT("Cannot create output buffer", hr);

        D3D11_UNORDERED_ACCESS_VIEW_DESC uavDesc{};
        uavDesc.Format              = DXGI_FORMAT_UNKNOWN;
        uavDesc.ViewDimension       = D3D11_UAV_DIMENSION_BUFFER;
        uavDesc.Buffer.FirstElement = 0;
        uavDesc.Buffer.NumElements  = 1;
        uavDesc.Buffer.Flags        = 0;

        hr = device->CreateUnorderedAccessView(outputBuffer.Get(), &uavDesc, &outputUAV);
        CHECK_HRESULT("Cannot create output buffer UAV", hr);
    }

    {
        D3D11_BUFFER_DESC bufferDesc{};
        bufferDesc.ByteWidth           = 12;
        bufferDesc.Usage               = D3D11_USAGE_STAGING;
        bufferDesc.BindFlags           = 0;
        bufferDesc.CPUAccessFlags      = D3D11_CPU_ACCESS_READ;
        bufferDesc.MiscFlags           = 0;
        bufferDesc.StructureByteStride = 12;

        hr = device->CreateBuffer(&bufferDesc, nullptr, &outputBufferCopy);
        CHECK_HRESULT("Cannot create output buffer", hr);
    }

    {
        D3D11_BUFFER_DESC bufferDesc{};
        bufferDesc.ByteWidth           = 12;
        bufferDesc.Usage               = D3D11_USAGE_STAGING;
        bufferDesc.BindFlags           = 0;
        bufferDesc.CPUAccessFlags      = D3D11_CPU_ACCESS_WRITE;
        bufferDesc.MiscFlags           = 0;
        bufferDesc.StructureByteStride = 12;

        hr = device->CreateBuffer(&bufferDesc, nullptr, &outputBufferClear);
        CHECK_HRESULT("Cannot create output clear buffer", hr);
    }

    FILE *csoFile = fopen("keybrute.cso", "rb");
    if (!csoFile) {
        SHOW_ERROR("cannot read keybrute.cso", errno);
    }

    fseek(csoFile, 0, SEEK_END);
    long fileSize = ftell(csoFile);
    fseek(csoFile, 0, SEEK_SET);

    auto *fileBuffer = new char[fileSize];
    if (fread(fileBuffer, 1, fileSize, csoFile) == -1) {
        fclose(csoFile);
        SHOW_ERROR("cannot read keybrute.cso", errno);
    }
    fclose(csoFile);

    Microsoft::WRL::ComPtr<ID3D11ComputeShader> cs;
    device->CreateComputeShader(fileBuffer, fileSize, nullptr, &cs);

    deviceContext->CSSetShader(cs.Get(), nullptr, 0);
    deviceContext->CSSetConstantBuffers(0, 1, inputBuffer.GetAddressOf());
    deviceContext->CSSetUnorderedAccessViews(0, 1, outputUAV.GetAddressOf(), nullptr);

    constexpr uint32_t NUM_THREADGROUPS = 32768;
    constexpr uint32_t THREADGROUP_SIZE = 1024;
    constexpr uint64_t KEY_INCREMENT    = (uint64_t)NUM_THREADGROUPS * (uint64_t)THREADGROUP_SIZE;

    uint64_t lastTime = GetTickCount64();

    uint32_t block1_a = packBytes(&tkure[0]);
    uint32_t block1_b = packBytes(&tkure[4]);
    uint32_t block2_a = packBytes(&tkure[8]);
    uint32_t block2_b = packBytes(&tkure[12]);

    uint64_t currentBlock;
    while (jobGroup.findNextBlock(currentBlock)) {
        uint64_t startingKey = currentBlock << 32ull;
        uint64_t endingKey   = (currentBlock + 1) << 32ull;
        for (uint64_t currentKey = startingKey; currentKey < endingKey; currentKey += KEY_INCREMENT) {
            {
                D3D11_MAPPED_SUBRESOURCE mappedBuffer;
                hr = deviceContext->Map(inputBuffer.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedBuffer);
                CHECK_HRESULT("Cannot map input buffer", hr);

                auto *data = static_cast<InputShaderData *>(mappedBuffer.pData);

                data->startingKey_hi = currentKey >> 32u;
                data->startingKey_lo = currentKey & 0xFFFFFFFF;
                data->block1_a       = block1_a;
                data->block1_b       = block1_b;
                data->block2_a       = block2_a;
                data->block2_b       = block2_b;

                deviceContext->Unmap(inputBuffer.Get(), 0);
            }

            deviceContext->Dispatch(NUM_THREADGROUPS, 1, 1);

            deviceContext->CopyResource(outputBufferCopy.Get(), outputBuffer.Get());

            {
                D3D11_MAPPED_SUBRESOURCE mappedBuffer;
                hr = deviceContext->Map(outputBufferCopy.Get(), 0, D3D11_MAP_READ, 0, &mappedBuffer);
                CHECK_HRESULT("Cannot map output buffer", hr);

                auto *dataRaw = reinterpret_cast<OutputShaderData *>(mappedBuffer.pData);
                OutputShaderData data{ *dataRaw };

                deviceContext->Unmap(outputBufferCopy.Get(), 0);

                if (data.status) {
                    printf("GPU FOUND KEY: %08x%08x\n", data.key_hi, data.key_lo);
                    printf("validating...\n");
                    fflush(stdout);

                    FILE *logFP = fopen("keys.log", "a");
                    fprintf(logFP, "GPU FOUND KEY: %08x%08x\n", data.key_hi, data.key_lo);
                    fflush(logFP);

                    uint64_t key = static_cast<uint64_t>(data.key_hi) << 32 | static_cast<uint64_t>(data.key_lo);
                    if (validateKey(tkure, key)) {
                        printf("FOUND KEY: %08x%08x\n", data.key_hi, data.key_lo);
                        fprintf(logFP, "FOUND KEY: %08x%08x\n", data.key_hi, data.key_lo);
                        fflush(logFP);

                        goto finished;
                    } else {
                        printf("key is invalid\n");
                        fprintf(logFP, "key is invalid\n");
                        fflush(logFP);

                        deviceContext->CopyResource(outputBuffer.Get(), outputBufferClear.Get());
                    }

                    fclose(logFP);
                }
            }
        }

        // ssm.markBlockExplored(currentBlock);

        uint64_t currentTime = GetTickCount64();
        uint64_t dt          = currentTime - lastTime;

        uint64_t calcAverage = jobGroup.additionalAverageByLastBatchTime(dt);

        constexpr size_t HUMAN_READABLE_BUFSIZE = 256;
        static char lastBatch[HUMAN_READABLE_BUFSIZE];
        static char averageBatch[HUMAN_READABLE_BUFSIZE];
        static char elapsedTime[HUMAN_READABLE_BUFSIZE];
        static char remainingTime[HUMAN_READABLE_BUFSIZE];

        // Estimate when this will be done
        uint64_t timeRemaining                                     = jobGroup.estimateTimeRemaining();
        std::chrono::time_point<std::chrono::system_clock> nowTime = std::chrono::system_clock::now();
        std::chrono::time_point<std::chrono::system_clock> endTime = nowTime + std::chrono::milliseconds(timeRemaining);
        std::time_t stdTime                                        = std::chrono::system_clock::to_time_t(endTime);
        char *stdTimeStr                                           = ctime(&stdTime);
        stdTimeStr[strlen(stdTimeStr) - 1]                         = '\0';

        printf(
            "processed batch %06llx, took %llums (ETA %s)\n\tcur=%20s avg=%20s left=%12llu\n\tela=%-27s rem=%-27s\n",
            currentBlock,
            dt,
            stdTimeStr,
            humanReadableUnits(lastBatch, HUMAN_READABLE_BUFSIZE, calculateBatchesPerSecond(dt), "Keys/s"),
            humanReadableUnits(averageBatch, HUMAN_READABLE_BUFSIZE, calculateBatchesPerSecond(calcAverage), "Keys/s"),
            jobGroup.getBlocksRemaining(),
            humanReadableTime(elapsedTime, HUMAN_READABLE_BUFSIZE, jobGroup.getElapsedTime()),
            humanReadableTime(remainingTime, HUMAN_READABLE_BUFSIZE, timeRemaining)
        );
        fflush(stdout);

        lastTime = currentTime;
    }

finished:
    if (rdocAPI) {
        rdocAPI->EndFrameCapture(device.Get(), nullptr);
        // sleep because RenderDoc's library does it asynchronously
        // and there's no way to wait for completion
        Sleep(1000);
    }

    return S_OK;
}

static bool extractNumber(const char *str, uint64_t &result) {
    errno        = 0;
    char *endptr = nullptr;
    result       = strtoull(str, &endptr, 16);
    if (endptr == str || ((result == LONG_MAX || result == LONG_MIN) && errno == ERANGE)) {
        return false;
    }
    return true;
}

HRESULT runApp(int argc, char **argv) {
    EnumAction action       = ACTION_RUN_BRUTEFORCE;
    int adapterIndex        = 0;
    uint64_t startingBlock  = 0ULL;
    uint64_t endingBlock    = 0x100'0000ULL;
    const char *keyFileName = nullptr;
    int flushFrequency      = 1;

    c2_init();

    for (int i = 1; i < argc; i++) {
        char *arg = argv[i];
        if (strcmp("-listadapters", arg) == 0) {
            action = ACTION_LIST_ADAPTERS;
        } else if (strcmp("-adapter", arg) == 0) {
            if (++i >= argc) {
                fprintf(stderr, "no argument for -adapter\n");
                return E_INVALIDARG;
            }
            adapterIndex = atoi(argv[i]);
        } else if (strcmp("-flush-frequency", arg) == 0) {
            if (++i >= argc) {
                fprintf(stderr, "no argument for -flush-frequency\n");
                return E_INVALIDARG;
            }
            flushFrequency = atoi(argv[i]);
        } else if (strcmp("-starting-block", arg) == 0) {
            if (++i >= argc) {
                fprintf(stderr, "no argument for -starting-block\n");
                return E_INVALIDARG;
            }

            if (!extractNumber(argv[i], startingBlock)) {
                fprintf(stderr, "invalid starting block %s\n", argv[i]);
                return E_INVALIDARG;
            }
        } else if (strcmp("-ending-block", arg) == 0) {
            if (++i >= argc) {
                fprintf(stderr, "no argument for -ending-block\n");
                return E_INVALIDARG;
            }

            if (!extractNumber(argv[i], endingBlock)) {
                fprintf(stderr, "invalid ending block %s\n", argv[i]);
                return E_INVALIDARG;
            }
        } else if (strcmp("-key-file", arg) == 0) {
            if (++i >= argc) {
                fprintf(stderr, "no argument for -key-file\n");
                return E_INVALIDARG;
            }

            keyFileName = argv[i];
        } else {
            fprintf(stderr, "unknown argument %s\n", arg);
            return E_INVALIDARG;
        }
    }

    Microsoft::WRL::ComPtr<IDXGIFactory1> factory;
    HRESULT hr = CreateDXGIFactory1(IID_PPV_ARGS(&factory));
    CHECK_HRESULT("Could not create DXGI factory", hr);

    if (action == ACTION_LIST_ADAPTERS) {
        listAdapters(factory);
    } else if (action == ACTION_RUN_BRUTEFORCE) {
        JobGroup jobGroup(startingBlock, endingBlock);

        if (!keyFileName) {
            fprintf(stderr, "-key-file is not specified\n");
            return E_INVALIDARG;
        }

        FILE *fp = fopen(keyFileName, "rb");
        if (!fp) {
            fprintf(stderr, "key file failed to open: %s\n", strerror(errno));
            return E_INVALIDARG;
        }

        uint8_t tkure[64];
        if (fread(tkure, 1, 64, fp) != 64) {
            fprintf(stderr, "key file failed to read: %s\n", strerror(errno));
            return E_INVALIDARG;
        }

        fclose(fp);

        Microsoft::WRL::ComPtr<IDXGIAdapter1> adapter;
        hr = factory->EnumAdapters1(adapterIndex, &adapter);
        CHECK_HRESULT("Cannot get DXGI adapter", hr);

        // Prevent shutdown while this is running
        SetProcessShutdownParameters(0x100, 0);

        hr = runBruteforce(factory, adapter, jobGroup, flushFrequency, tkure);
        CHECK_HRESULT("Could not run bruteforce", hr);
    }
    return S_OK;
}

int main(int argc, char **argv) {
    return SUCCEEDED(runApp(argc, argv)) ? EXIT_SUCCESS : EXIT_FAILURE;
}
