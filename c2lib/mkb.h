#pragma once

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif
typedef struct {
    uint8_t col;
    uint16_t row;
    uint64_t key;
} device_key_t;

extern device_key_t *cprm_device_keychains[];

int cprm_process_mkb(uint8_t *p_mkb, device_key_t *p_dev_keys, int nr_dev_keys, uint64_t *p_media_key);

#ifdef __cplusplus
}
#endif
