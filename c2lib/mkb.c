#include "mkb.h"

#include "c2.h"
#include "pgl_log.h"
#include <stdbool.h>

static device_key_t cprm_device_keys_mkb9[] = { // keychain from Samsung SC-02C
    { 0x00, 0x0025, 0xFDA351C576427E },
    { 0x01, 0x0018, 0x924E928633CF37 },
    { 0x02, 0x0156, 0x81F8AE720AC8F1 },
    { 0x03, 0x01E7, 0xF468D10C4AF135 },
    { 0x04, 0x01EE, 0x13AE099D3BDBA1 },
    { 0x05, 0x01D1, 0xC3CFACB8DF3DCE },
    { 0x06, 0x01B6, 0xA7820B380D5F87 },
    { 0x07, 0x014F, 0xE9E7EA944D44A1 },
    { 0x08, 0x0029, 0x0A324D982130D6 },
    { 0x09, 0x01CB, 0x215A9FD080A04C },
    { 0x0A, 0x01B4, 0x527853F75D7377 },
    { 0x0B, 0x01DB, 0xC5359A7564E0AB },
    { 0x0C, 0x01C1, 0x990724887C8FB9 },
    { 0x0D, 0x00C5, 0x3DC6EF19521F97 },
    { 0x0E, 0x01F0, 0xCD00339C3EA918 },
    { 0x0F, 0x016A, 0xF793817564BD1E },
    // end
    { 0xFF, 0xFFFF, 0xFFFFFFFFFFFFFF }
};

static device_key_t cprm_device_keys_mkb11[] = {
    { 0x00, 0x002A, 0xd3363c479f7de5 }, // bruteforced thanks to GoodTofuFriday ^_^
    // end
    { 0xFF, 0xFFFF, 0xFFFFFFFFFFFFFF }
};

device_key_t *cprm_device_keychains[] = {
    NULL, // MKB0
    NULL, // MKB1
    NULL, // MKB2
    NULL, // MKB3
    NULL, // MKB4
    NULL, // MKB5
    NULL, // MKB6
    NULL, // MKB7
    NULL, // MKB8
    cprm_device_keys_mkb9, // MKB9
    NULL, // MKB10
    cprm_device_keys_mkb11, // MKB11
    NULL, // MKB12
    NULL, // MKB13
    NULL, // MKB14
    NULL, // MKB15
    NULL, // MKB16
};

int cprm_process_mkb(uint8_t *p_mkb, device_key_t *p_dev_keys, int nr_dev_keys, uint64_t *p_media_key) {
    uint64_t offset   = 0;
    bool hasMediaKey  = false;
    uint64_t mediaKey = 0, verificationData = 0;
    while (offset < 65536) {
        uint8_t recordType = p_mkb[offset++];
        uint32_t length    = 0;
        length |= p_mkb[offset++] << 16;
        length |= p_mkb[offset++] << 8;
        length |= p_mkb[offset++] << 0;
        length -= 4;

        uint64_t buffer = 0;
        if (length >= 8) {
            buffer = 0;
            buffer |= (uint64_t)p_mkb[offset++] << 56;
            buffer |= (uint64_t)p_mkb[offset++] << 48;
            buffer |= (uint64_t)p_mkb[offset++] << 40;
            buffer |= (uint64_t)p_mkb[offset++] << 32;
            buffer |= (uint64_t)p_mkb[offset++] << 24;
            buffer |= (uint64_t)p_mkb[offset++] << 16;
            buffer |= (uint64_t)p_mkb[offset++] << 8;
            buffer |= (uint64_t)p_mkb[offset++] << 0;
            length -= 8;
        }

        uint8_t column;
        device_key_t *devkey = NULL;

        switch (recordType) {
        case 0x82:
            buffer = c2_dec(buffer, mediaKey);
            if ((buffer & 0xffffffff00000000) != 0xdeadbeef00000000) {
                PGL_LOG("invalid key for conditional");
                break;
            }
            // fallthrough
        case 0x01:
            column = (buffer >> 24) & 0xFF;
            if ((buffer & 0xFFFFFF) != 0x000001 || column > 16) {
                break;
            }


            // find device key for this col
            for (int i = 0; i < nr_dev_keys; i++) {
                if (p_dev_keys[i].col == 0xFF) {
                    devkey = NULL;
                    break;
                }
                if (p_dev_keys[i].col == column) {
                    devkey = &p_dev_keys[i];
                    break;
                }
            }
            if (!devkey) {
                PGL_LOG("devkey not found for col %d\n", column);
                break;
            }

            if (((devkey->row + 1u) * 8u) > length) {
                PGL_LOG("not enough rows\n");
                break;
            }

            offset += devkey->row * 8;

            buffer = 0;
            buffer |= (uint64_t)p_mkb[offset++] << 56;
            buffer |= (uint64_t)p_mkb[offset++] << 48;
            buffer |= (uint64_t)p_mkb[offset++] << 40;
            buffer |= (uint64_t)p_mkb[offset++] << 32;
            buffer |= (uint64_t)p_mkb[offset++] << 24;
            buffer |= (uint64_t)p_mkb[offset++] << 16;
            buffer |= (uint64_t)p_mkb[offset++] << 8;
            buffer |= (uint64_t)p_mkb[offset++] << 0;
            length = length - 8 - devkey->row * 8;

            if (recordType == 0x82) {
                buffer = c2_dec(buffer, mediaKey);
            } else if (hasMediaKey) {
                break;
            }

            mediaKey = c2_dec(buffer, devkey->key) & 0xFFFFFFFFFFFFFFull;
            buffer   = c2_dec(verificationData, mediaKey);

            hasMediaKey = true;

            if ((buffer & 0xffffffff00000000ull) == 0xdeadbeef00000000ull) {
                PGL_LOG("found a valid media key");
                *p_media_key = mediaKey;
                return 0;
            }
            break;
        case 0x02:
            return -1;
        case 0x81:
            verificationData = buffer;
            break;
        default:
            break;
        }

        offset += length;
    }
    return -1;
}
